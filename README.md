# Lab5 -- Integration testing

## Lab
1. Create your fork of the `
Lab5 - Integration testing
` repo and clone it. [***Here***](https://gitlab.com/romanza705/s-23-lab-5-integration-testing-r-mukhtarov)

2. Try to use InnoDrive application:

`https://script.google.com/macros/s/AKfycby1s4fIXUfPk8Oxue9RhFZ2_Tq2m-P2Bz5CapR3eo8KHd_c4qXwVyWfgKxwM-ZsnY8U/exec?service=getSpec&email=r.mukhtarov@innopolis.university`

            
    -         Here is InnoCar Specs:
              Budet car price per minute = 21
              Luxury car price per minute = 42
              Fixed price per km = 17
              Allowed deviations in % = 13
              Inno discount in % = 10

- To get the price for certain car ride, let's send next request:

## BVA Table

| Parameter        | Equivalence Class |
| --------         | -------- |
| type             | luxury; budget; nonsense; empty |
| plan             | minute; fixed_price; nonsense; empty |
| distance         | <= 0; > 0; empty |
| planned_distance | <= 0; > 0; empty |
| time             | <= 0; > 0; empty |
| planned_time     | <= 0; > 0; empty |
| inno_discount    | yes; no; nonsense; empty |

where `empty` means not specified.

## Decision Table

|     | `type` | `plan` | `distance` | `planned_distance` | `time` | `planned_time` | `inno_discount` | `Result` |
|-----| -------- | -------- | -------- | -------- | -------- | -------- | -------- | -------- |
| T1  | nonsense | * | * | * | * | * | * | Invalid |
| T2  | * | nonsense | * | * | * | * | * | Invalid |
| T3  | * | * | <= 0 | * | * | * | * | Invalid |
| T4  | * | * | * | <= 0 | * | * | * | Invalid |
| T5  | * | * | * | * | <= 0 | * | * | Invalid |
| T6  | * | * | * | * | * | <= 0 | * | Invalid |
| T7  | * | * | * | * | * | * | nonsense | Invalid |
| T8  | empty | * | * | * | * | * | * | Invalid |
| T9  | * | empty | * | * | * | * | * | Invalid |
| T10 | * | * | * | * | * | * | empty | Invalid |
| T11 | luxury | minute | > 0 | > 0 | > 0 | > 0 | yes | Valid |
| T12 | luxury | minute | > 0 | > 0 | > 0 | > 0 | no | Valid |
| T13 | luxury | minute | > 0 | > 0 | empty | > 0 | * | Invalid |
| T14 | luxury | minute | empty | empty | > 0 | > 0 | yes | Valid |
| T15 | luxury | minute | empty | empty | > 0 | > 0 | no | Valid |
| T16 | luxury | fixed_price | > 0 | > 0 | > 0 | > 0 | yes | Invalid |
| T17 | luxury | fixed_price | > 0 | > 0 | > 0 | > 0 | no | Invalid |
| T18 | budget | minute | > 0 | > 0 | > 0 | > 0 | yes | Valid |
| T19 | budget | minute | > 0 | > 0 | > 0 | > 0 | no | Valid |
| T20 | budget | minute | > 0 | > 0 | empty | empty | * | Invalid |
| T21 | budget | fixed_price | > 0 | > 0 | empty | empty | no | Invalid |
| T22 | budget | fixed_price | > 0 | > 0 | empty | empty | yes | Invalid |
| T23 | budget | fixed_price | empty | empty | > 0 | > 0 | * | Invalid |


## Running test cases

I run the following test cases:

| # Test case | Link | Expected result | Actual result | Passed |
| -------- | -------- | -------- | -------- | -------- |
| T1  |  type=nonsense plan=minute distance=1 planned_distance=1 time=1 planned_time=1 inno_discount=yes | Invalid Request | Invalid Request | ✅ |
    | T2  |  type=budget plan=nonsense distance=1 planned_distance=1 time=1 planned_time=1 inno_discount=yes | Invalid Request | Invalid Request | ✅ |
    | T3  |  type=budget plan=minute distance=-1 planned_distance=1 time=1 planned_time=1 inno_discount=yes | Invalid Request | Invalid Request | ✅ |
    | T4  |  type=budget plan=minute distance=1 planned_distance=-1 time=1 planned_time=1 inno_discount=yes | Invalid Request | Invalid Request | ✅ |
    | T5  |  type=budget plan=minute distance=1 planned_distance=1 time=-1 planned_time=1 inno_discount=yes | Invalid Request | Invalid Request | ✅ |
    | T6  |  type=budget plan=minute distance=1 planned_distance=1 time=1 planned_time=-1 inno_discount=yes | Invalid Request | {"price":14.7} | ❌ |
    | T7  |  type=budget plan=minute distance=1 planned_distance=1 time=1 planned_time=1 inno_discount=nonsense | Invalid Request | Invalid Request | ✅ |
    | T8  |  plan=minute distance=1 planned_distance=1 time=1 planned_time=1 inno_discount=yes | Invalid Request | Invalid Request | ✅ |
    | T9  |  type=budget distance=1 planned_distance=1 time=1 planned_time=1 inno_discount=yes | Invalid Request | Invalid Request | ✅ |
    | T10 |  type=budget plan=minute distance=1 planned_distance=1 time=1 planned_time=1  | Invalid Request | Invalid Request | ✅ | 
    | T11 |  type=luxury plan=minute distance=1 planned_distance=1 time=20 planned_time=20 inno_discount=yes | {"price":982.8} | {"price":1092} | ❌ |
    | T12 |  type=luxury plan=minute distance=1 planned_distance=1 time=20 planned_time=20 inno_discount=no  | {"price":1092} | {"price":1092} | ✅ |
    | T13 |  type=luxury plan=minute distance=1 planned_distance=1 planned_time=20 inno_discount=yes | Invalid Request | {"price":null} | ❌ |
    | T14 |  type=luxury plan=minute time=20 planned_time=20 inno_discount=yes | {"price":982.8} | {"price":1092} | ❌ |
    | T15 |  type=luxury plan=minute time=20 planned_time=20 inno_discount=no | {"price":1092} | {"price":1092} | ✅ |
    | T16 |  type=luxury plan=fixed_price distance=1 planned_distance=1 time=20 planned_time=20 inno_discount=yes | Invalid Request | Invalid Request | ✅ |
    | T17 |  type=luxury plan=fixed_price distance=1 planned_distance=1 time=20 planned_time=20 inno_discount=no | Invalid Request | Invalid Request | ✅ |
    | T18 |  type=budget plan=minute distance=1 planned_distance=1 time=5 planned_time=20 inno_discount=yes | {"price":66.15} | {"price":73.5} | ❌ |
    | T19 |  type=budget plan=minute distance=1 planned_distance=1 time=5 planned_time=20 inno_discount=no | {"price":73.5} | {"price":73.5} | ✅ |
    | T20 |  type=budget plan=minute distance=1 planned_distance=1 planned_time=20 inno_discount=no | Invalid Request | {"price":null} | ❌ |
    | T21 |  type=budget plan=fixed_price distance=5 planned_distance=5 inno_discount=no | Invalid Request | {"price":null} | ❌ |
    | T22 |  type=budget plan=fixed_price distance=5 planned_distance=5 inno_discount=yes | Invalid Request | {"price":null} | ❌ |
    | T23 |  type=budget plan=fixed_price time=10 planned_time=10 inno_discount=yes | Invalid Request | {"price":166.66666666666666} | ❌ |


## Results

I run all test cases and investigated the following problems(bugs):

* There is no validation of parameter `planned_time`(negative value can be passed).
* The price is incorrectly calculated for luxury and budget cars in minute plan with discount.
* The price is incorrectly calculated for luxury in case of absence of time parameter.